#!/bin/bash -x
export ENABLE_PDF_EXPORT=1,
PWD=`pwd`
/usr/bin/virtualenv --python=python3 venv
echo $PWD
activate () {
    . $PWD/venv/bin/activate
}

activate
pip install mkdocs mkdocs-pdf-export-plugin pydyf pdfje 
cd docs 
mkdocs build --clean