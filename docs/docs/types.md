
# Process

Simple casting system processes;
```
proc procname "command" "work path" ["arg1","arg2"]
```

# Socket

Simple casting system sockets;
```
sock socketname "type" "port" "ip"
```

# Literals

The literal definition is used to describe the system invariants. 

The literal definition is used to describe the system invariants. These definitions maintain their validity throughout the system. Similar to definitions. There is no need to use a parameter for a single variable. But for multiple variables, brackets and commas are the same as in functions.

```
literal functionname(args){
    ....
}
```

Example;
```
literal db(name){
	createdb(name)
}
db "database"
literal writedb(name, key, data){
	insertdb(name, key, data)
}
writedb ("database", "olang", "Hello olang data struct!")
```


# Scopes

The scope structure is similar to class structures. All the functions defined in scope form subfunctions of a scope. In this way you can clustering by writing functions in the main scope.

```
scope scopename {
    .... internal functions
}
```

Scope definition example:
```
scope example { 
  def hello = fn(x){ 
    return ("Hello "+x)
  }; 
  def subone = fn(x){ 
    return (x + 1)
  }; 
}
```
Scope call:
```
def hello = example::hello("Oytun")
def number = example::subone(1)
show(hello)
show(number)
```
Scope output:
```
Hello Oytun
2
```
