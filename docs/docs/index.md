# Getting Started

First get `olang(.exe)` file or `go get -v gitlab.com/olanguage/olang` and run `olang(.exe) (-r) <filename> (-d=true [debug mode])`

Thats it.


Create file with example.ola;

```
output("Hello Galaxy!")
```

Run file with olang;
```
olang example.ola
```

Result;
```
"Hello Galaxy!"
```

Thats all.


# Files

Olang files is to simple. Default file extension “.ola”. Let’s start by examining the programming language example.

sample test.ola file:

```
def hello = fn(name){
  return ("Hello, "+name+"! Welcome to O-lang!")
}
def result = hello("Oytun")
output(result)
```

Run:

```
$ olang test.ola
```

Output:

```
Hello, Oytun! Welcome to Olang
```

# Module Loading

O Language files is to simple. File extension “.olm”. Let’s start by examining the programming language example.



hello.olm (as hello module):
```
def hello = fn(name){ 
  output("Hello, "+name+"! Welcome to Olang")
}
```
load.ola (main file):
```
load "hello.olm"
def name = "Oytun";
hello(name);
```
Run:
```
$ olang load.ola
```
Output:
```
Hello, Oytun! Welcome to Olang
```

# Types

- Olang Singe File (.ola) - olang/single-file
- Olang Module File (.ols) - olang/scope-file
- Olang Module File (.olm) - olang/module-file
- Olang Process File (.olproc) - olang/process-file
- Olang Program File (.olprog) - olang/program-file
- Olang Library File (.olibrary) - olang/library-file
- Olang Compresed File (.olc) - olang/compresed-file
- Olang Packaging File (olpfile.json) - olang/olpfile
- Olang Process File (Opsfile) - olang/opsfile


# What is SOFPL?

System Oriented Functional Programming Language (SOFPL) is a specific structure that interprets each line separately and outputs the result. Shorten the functions performed in normal programming languages. For example, you can do the functions you can do in 10 lines with a line. It is up to you to determine what you will do in 9 lines. :)

In addition, you can start the process on the system, you can open the port and start data transfer. Simple socket software and transaction management functions are available and developed.

Use single line save for lines

C lang Hello World example;

```
# include < stdio.h >
int main()
{
   // printf() displays the string inside quotation
   printf("Hello, World!");
   return 0;
}
```

With the C programming language you have spent 7 lines to write the hello world.


Hello World example;

```
def hello = fn(name){
    return ("Hello, "+name+"! Welcome to O-lang!")
}
hello("Oytun")
```

Without function write as:

```
"Hello, World!"
```
Print function write as:

```
output("Hello, World!")
```
or:
```
println("Hello, World!")
```

You can do this in just one line. Save more time with Olang. The SLFPL structure saves you a lot of lines.

Socket is suitable for programming and transaction management.

You can run system processes directly and then write applications on the results.

dial_serve.ola (dial serve example):
```
sock socket "tcp4" "9050" "0.0.0.0";
def messages = {
  "ping": "pong",
  "call": "Calling... NOPE! :)",
  "help": "Not helpfull!"
}
dial_service(socket, messages);
```

The 9050 port is ready to connect.

dial_client.ol (dial client example):

```
sock socket "tcp4" "9050" "0.0.0.0";
def send = fn(message){
  println(dial_send(socket, message))
}
send("call")
send("help")
```

Output:

```
Calling... NOPE! :)
Not helpfull!
```

In this example, a dial was made. Runs a socket directly on the system and exchanges messages.

Code:

```
proc ls "ls" "/home/olang-home" ["-alh"]
env "TEST" "test" 
rintln(sysenv("TEST"))
println(ls)
```

Result:

```
test
...folder contents....
In this example, a process is set up and output is provided. In addition, we have defined the system variable and let it do so.
```