# fn
```
Function: def object = fn(...){....}
```
# def
```
def object = something
```
# true
```
Boolean: true
```
# false
```
Boolean: false
```
# if
```
if(...boolean){ .... }
```
# else
```
else{ .... }
```
# return
```
def pi = fn(x){** return "3.14" **}
```
# input
```
input(object)
```
# output
```
output(object)
```
# loop
```
loop(condition){...result}
def result = (loop(condition){return ...result})
....results array or object
```
Example;
```
def i = 0;
loop(i>100){
  output(i);
  def i = (i+1);
}
...results...
```

# while
```
while(condition){...result}
def result = (while(condition){return ...result})
....results array or object
```
Example;
```
def i = 0;
while(i<100){
  output(i);
  def i = (i+1);
}
...results...
```
# for

For loop generates results from loops
```
def arr = [1,2,3,4,5]
for(arr in k,v){
  output(v)
}
def arrtwo = {"one":1, "two":2};
for(arrtwo in k,v){
  output(k)
  output(v)
}
...results...
```
# load

Load some olang module, file etc:
```
load "filename"
```
# scope

Create scope for functions or variables:
```
scope scopename {
  def scopevar = somevariable;
  def scopefn = fn(....){....}
}
scopename::scopevar
scopename::scopefn(....)
```
# proc

Create system process;
```
proc procname "command" "work path" ["arg1","arg2"]
....process result
```
# sock

Create system socket;
```
sock socketname "type" "port" "ip"
...sock result
```
# env

Create system environment value or change;
```
env "type" "value"
getenv(result)
....value
```