# Deprecated Functions And Documents

This functions removed from newer versions. Not maintained yet. Archive only. :/

- router
- dial_service
- dial_send
- proc_exec
- createdb
- insertdb
- getdb
- deletedb
- searchdb
- http
- https
- show
- println

# router (deprecated)

Router definition function. Simple create router use for web servers or micro servers.
```
router(port, hash definition);
```
Example;
```
def config = {
  "/": {
    "type": "GET",
    "response": "Hello!",
  },
};
router("8080", config)
```
Defined new route with this example;
```
GET http://0.0.0.0:8080/
"Hello!"
```

# dial_service (deprecated)

Create dial messaging service from socket. All requests and response need to hash iteration.
```
sock socket "tcp4" "9050" "0.0.0.0";
def messages = {"ping": "pong"}
dial_service(socket, messages);
```
Created dial service.

# dial_send (deprecated)

Send messages to dial_service
```
sock socket "tcp4" "9050" "0.0.0.0"
dial_send(socket, "ping")
```
Returns “pong” called from dial_service.

# proc_exec (deprecated)

Execute some system command in background and return Process object.
```
proc_exec(command strings...)
```
Example;
```
proc_exec("ls -al")
Files....
```

# createdb (deprecated)

Creates new database with inline named folder.
```
createdb(dbname)
```
Created new key based database in current folder.

# insertdb (deprecated)

Insert database something.
```
insertdb(dbname, key, value)
```
# getdb (deprecated)

Get database with some key.
```
getdb(dbname, key)
```

# deletedb (deprecated)

Delete database with some key.
```
deletedb(dbname, key)
```
# searchdb (deprecated)

Directly search all database with get all objects.
```
searchdb(dbname, some string)
```

# http (deprecated)

Runs http server with string or rendered objects.
```
http(routepath, port, string object);
```

# https (deprecated)

Runs http server with string or rendered objects.
```
http(route path, port, string object, crt key, server key);
```


# Create Http/Https Sockets (deprecated)

You can use the http(path, port, response) or https(path, port, response, servercrt, serverkey) function to easily create an http socket.


httptest.ol:
```
show("Listening 0.0.0.0:8080... Ctrl+C to exit.")
http("/", "8080", "Hello, Olang World from Web!");
```
httpstest.ol:
```
show("Listening 0.0.0.0:8443... Ctrl+C to exit.")
https("/", "8443", "Hello, Olang World from Web!", "server.crt", "server.key");
```
First we loaded our "hello” module with load. Later in the hello() function on the 8080 or 8443 (needs; server.crt and server.key)port to provide this output. When we enter port localhost:8080 or localhost:8443 (needs; server.crt and server.key) from our browser or curl, the result will be;
```
~> curl localhost:8080
Hello, Oytun! Welcome to Olang
```

If we are going to do a test to render HTML, we can write the HTML module first so that we can link it to the http socket and render it.

httprenderexample.ol (HTTP socket with HTML module => main):
```
show("Listening 0.0.0.0:8080... Ctrl+C to exit.");
http("/",
  "8080",
  "Hello, Olang World from Web!")
);
```
I started a sample http socket by including the jquery script and other plugins in my html parameters. As a result, when I call the socket with curl;
```
~> curl localhost:8080
.....content....
```
I gave an HTML output as a conversion. This will be similarly rendered on the Web.