# Make Router

The router socket structure was created with the intention of creating an API over the HTTP/S protocol and exchanging data through the socket. In web based projects it is possible to write web server quickly and easily. In addition, the API can be created in micro-services.

hello.olm (as hello module):
```
def hello = fn(name){
  return ("Hello, "+name+"! Welcome to Olang")
}
```
router.ol (as router):
```
load "hello.olm"
def config = {
  "/": {
    type: "GET",
    response: "Welcome to the O-Lang Router!"
  },
  "/:name":{
    type: "POST",
    response: (hello("{{.name}}"))
  }
}
show("Listening 0.0.0.0:8080... Ctrl+C to exit.")
webserver("8080",  config);
```
GET method test:
```
~> curl -X GET localhost:8080
Welcome to the O-Lang Router!
```
POST method test (oytun as parameter value for name [ :name => {{.name}} ]):
```
~> curl -X POST localhost:8080/oytun
Hello, oytun! Welcome to Olang
```
As you can see in the example, we wrote a web micro service using port 8080 via get and post methods.


# Dialers

Dialer functions can automatically initiate a socket or provide sockete access and automatically communicate over a socket.


dial_serve.ola (dial serve example):
```
sock socket "tcp4" "9050" "0.0.0.0"; 

def messages = { 
   "ping”: "pong”, 
   "call”: "Calling… NOPE! :)”, 
   "help”: "Not helpfull!” 
} 

sock_listen(socket, messages)
```

The 9050 port is ready to connect.

dial_client.ol (dial client example):
```
sock socket "tcp4" "9050" "0.0.0.0";
def send = fn(message){
  output(sock_send(socket, message))
} 
send("call")
send("help")
```
Output:
```
Calling... NOPE! :)
Not helpfull!
```