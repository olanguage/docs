# itostr

Convert integer to string. But strings and integers not connect with plus iteration (Ex: “Hello”+0 => imposible need to convert 0 or “Hello” to string or integer in IRL[In real life]).

For example;
```
def i = 0;
# => 0
def str = itostr(i)
# => "0"
```

# len

Gives something to length. Sometimes usefull for length calculation. Easy to use.

Example;
```
def arr = [1,2,3,4,5]
def lenArr = len(arr)
# => 5
```

# first

All objects gives first item. Thats all.

Example;
```
def hsh = {"x":10, "y":20, "z":30}
def fhsh = first(hsh)
{"x":10}
def arr = [1,2,3,4,5]
def farr = first(arr)
1
```

# last

All objects gives last item. Thats all.

Example;
```
def hsh = {"x":10, "y":20, "z":30}
def fhsh = last(hsh)
# => {"z":30}
def arr = [1,2,3,4,5]
def farr = last(arr)
# => 5
```

# rest

Extract first element from arrays;
```
def arr = [1,2,3,4,5]
len(arr)
5
rest(arr)
len(arr)
4
arr
[2,3,4,5]
```
# push

Add element to arrays;
```
def arr = [1,2,3,4,5]
push(arr, 6)
[1,2,3,4,5,6]
```

# output

Print every type convert string and show one line;
```
def hello = "Hello";
output(hello)
# => "Hello\n"
```

# input

Input from user;
```
def name = (input("Hello>"))
output(("Hello " + name + "!"))
"Hello> Oytun<enter>"
"Hello Oytun!"
```

# colorize

This is colorize function colorize some object and show directly. Run only ASCII consoles. You need to enabled colors.
```
colorize(colorstring, string, background:true|false)
```
Colorlist:
```
red
blue
green
yellow
grey
black
bold
magenta
white
```

# file

Opens some file gives content directly.
```
def definition = file(file name)
```
Example;
```
def hellofile = file("hello.txt")
"This is test file content for example"
```
# atostr

Gives some arrays from some key with string object.

Example;
```
def arr = [1,2,3,4,5]
atostr(arr, 0)
"1"
```


# database

Create or connect to database.

```
def db = database(type (sqlite3|mssql|postgres|mysql|internal), connection string)
```

# model

Create model for database connection.

```
def modeldefinition = (model(db, "tablename", {
	... definitions ...
}))
```

Definitions changes your database type.

# migrate

Migrate model to database or update.

```
migrate(modeldefinition)
```

# fetch

Fetch data with hash (this is where section). 

```
fetch(modeldefinition, {....where query hash...})
```

# update

Update data with where and update hash.

```
update(modeldefinition, {...where query hash... }, {....update query hash...})
```

# create

Create data with create query.

```
create(modeldefinition, {....create query hash...})
```

# delete

Delete data with delete query hash.

```
delete(modeldefinition, {...delete query hash...})
```

# drop

Drop model from database. But model definition is already exist for other ussage.

```
drop(modeldefinition)
```

# truncate

Clear table data with model.

```
truncate(modeldefinition)
```

# query

Run raw query with hash data (has data automaticaly filtered with this function for security).

```
query(modeldefinition, rawquery, {...hash data...})
```

# getval

Get some value converts to string. After automaticaly show or define value.

Example;
```
def x = 12;
getval(x)
"12"
```
# jsonp

Automaticaly parse json string and return hash object.

Example;
```
def js = "{val1:1,val2:2}"
jsonp(js)
{"val1":1, "val2":2}
```
# xmlp

Convert all xml string to hash or array object

Example;
```
def xmlo = "<arr><type1>1<type1><type2>2<type2><type3>2<type3></arr>"
xmlp(xmlo)
{"arr":{"type1":1,"type2":2,"type3":3}}
```
# wdir

Return current working directory.
```
wdir()
"/home/oytun/olang/"
```

# chdir

Change directory and return changed directory.
```
chdir("/home/oytun/olang/")
"/home/oytun/olang/"
```

# renderf

File render function. Some objects to render some file and gives string.

Example;

file.txt:
```
{{.output}}
```
script:
```
renderf("file.txt", {"output": "Olang Render Example"})
```
result:
```
Olang Render Example
```

# sysenv

Shows system environment tables or set some environment variable
```
sysenv(some system environment string, ...some system environment string)
```
Example;
```
sysenv("PATH")
/usr/local/bin:/bin:/usr/bin
```

# inspect

This is debugging function for variables.
```
inspect(...objects)
```
Example;
```
def x = 1;
inspect(x)
1
```
# regexp_check

Check regexp and string return bool object result.
```
def rex = "[A-Z][a-z]"
def matched = regexp_check(rex, "Hello!")
true
```
# regexp_find

Find all regexp results and return array object.
```
def rex = "el"
def matched = regexp_find(rex, "Hello!")
["el"]
```


# replace

Replaces the text in the text with the specified text. It takes three parameters and all are text.

```
replace(text, findtext, changetext)
```

# asm

Run inline assembly code.

```
asm(operationcode, destination, other args...)
```

Example;
```
asm("MOV", "AX", "10")
# => no output
```

# syscall

This was added although dangerous. Function to call up system properties. It is the syscall function in C.

```
syscall(trap, a1, a2, a3)
# => syscall result
```

The result represents the syscall result from the system. A function can be made to show these results in the future.


# rmdir

Remove a directory.

```
rmdir(folder)
```

# mkdir

Make a directory.

```
mkdir(folder)
```


# finfo

Show file information. 

```
finfo(folder)
```


# chmod

Change file permissions. 

```
chmod(fileOrFolder, unixPermission)
```


# remove

Remove a file. 

```
remove(file)
```


# write


Write a file. 

```
write(file, content)
```


# read

Read a file and return content. 

```
read(file)
```

# webserver

Web server definition function. Simple create web server use for web servers or micro servers.

Sample config with definitions (all in one);

```
def config = {
  "path": {
    type: "type of method (GET|POST|PUT|DELETE|PATCH|OPTIONS|HOST|UPLOAD|STATIC|STREAM)",
    response: "response string....",
    input: "input name"
    maxsize: "maximum upload size"
    headers: {...headers...}
    folder: "upload or static path folder"
    data: {...data....}
    host: "hostname or ip address for access"
  }
}
```

Sample config with definitions by type;

- GET

Simple get method. Get with response or data.

```
"/hello": {
  type: "GET",
  response: "Hello World!"
  data: {}
}
```

- POST

Simple post method. Post with response or data.

```
"/sayname": {
  type: "POST",
  response: "Hello {{.name}}!"
  data: {
    name: "oytun"
  }
}
```

- PUT

Simple put method. Put with response or data.

```
"/sayname": {
  type: "PUT",
  response: "Hello {{.name}}!"
  data: {
    name: "oytun"
  }
}
```

- DELETE

Simple delete method. Delete with response or data.

```
"/deletename": {
  type: "DELETE",
  response: "Hello {{.name}}!"
  data: {
    name: ""
  }
}
```

- PATCH

Simple patch method. Patch with response or data.

```
"/patchname": {
  type: "PATCH",
  response: "Hello {{.name}}!"
  data: {
    name: "olang"
  }
}
```

- OPTIONS

Simple options method. Options with response or data.

```
"/option": {
  type: "OPTIONS",
  response: "Name Option {{.name}}!"
  data: {
    name: "olang"
  }
}
```

- HOST

This is not method. Change host only.

```
"/": {
  type: "HOST",
  host: "127.0.0.1"
}
```

- UPLOAD (as PUT)

Upload files with PUT method to some folder. 

```
"/upload": {
  type: "UPLOAD",
  input: "files",
  maxsize: "512",
  folder: "upload/"
}
```

As api call `PUT /upload` with file input works great. UPLOAD method not exist in HTTP/2 standart.

- STATIC

Server all static files from path. General use in assets, public files etc.

```
"/public/*filepath":{
    "type": "STATIC",
    "response": "/public/*filepath"
  }

```

- STREAM

Stream file with get method from folder.

```
"/stream/{filename}": {
  type: "GET",
  folder: "files/{filename}"
}
```

```
webserver(port, config);
```


Example;
```
def config = {
  "/": {
    "type": "GET",
    "response": "Hello!",
  },
};
webserver("8080", config)

```
Defined new web server with this example;

```
GET http://0.0.0.0:8080/
"Hello!"
```

# rand

Generate random number with count.

```
rand(number)
```

# jsonf

Read json file and return array or hash.

```
jsonf(filename)
# => return has or string
```

# xmlf

Read xml file and return array or hash.

```
xmlf(filename)
# => return has or string
```

# chdir

Change current directory.

```
chdir(folder)
```

# sh_exec

Run some command with "sh -c" system command and return result. Execute some system command in background and return Process object.

```
sh_exec(command, attr)
```

Example;
```
sh_exec("ls", "-al")
Files....
```

# cmd_exec

Run some command with "cmd.exe /c" system command and return result. Execute some system command in background and return Process object.

```
cmd_exec(command, attr)
```

Example;
```
cmd_exec("dir")
Files....
```

# proc_kill

Kill some proccess with system pid
```
proc_kill(pid integer only)
```
Example;
```
proc_kill(330)
...killed 330 pid application or service
```

# proc_out

Executed process show or define output
```
proc example "ls" "/home/olang" ["-lha"]
proc_out(example)
.....files
```

# proc_pid

Executed process pid return
```
proc example "ls" "/home/olang" ["-lha"]
proc_pid(example)
12345 (proc pid)
```

# sock_listen

Create dial messaging service from socket. All requests and response need to hash iteration.
```
sock socket "tcp4" "9050" "0.0.0.0";
def messages = {"ping": "pong"}
sock_listen(socket, messages);
```
Created dial service.

# sock_send

Send messages to dial_service
```
sock socket "tcp4" "9050" "0.0.0.0"
sock_send(socket, "ping")
```
Returns “pong” called from dial_service.

# quit

Quit from olang
```
quit()
```