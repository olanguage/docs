# XML Parser

The xmlp() function is used to parse the XML. It is extremely simple to use. Returns the result as a hash.
```
def result = (xmlp("test.xml"))
output(result)
```
Output:
```
...xml content...
```

# URL Fetch/Extract

The urlfetch(<type (GET, POST)>, <url>, <data for POST>) and urlextract (<url object>, <option>) functions execute URL operations for data retrieval and destructor functions.

urlfetch.ola:
```
def olang = (urlfetch("GET","http://o-lang.tk"))
def type = (urlextract(olang, "content-type"))
def length = (urlextract(olang, "content-length"))
def server = (urlextract(olang, "server"))
def body = (urlextract(olang, "body"))

output(type)
output(length)
output(server)
output(body)
```

Output:
```
....content....
```


# File Render

The renderf() function is used to combine and render files with parameters. It’s simple to use. Returns the result as a string.
```
def filerender = (renderf(
   "test.html", 
   { 
       "hello": "World", 
       "title": "Hello Example" 
   }
))
output(filerender)
```
Input:
```
<html>
    <head>
        <title>{{.title}}!</title>
    </head>
    <body>
        <h1>{{.hello}}!</h1>
    </body>
</html>
```
Output:
```
<html>
    <head>
        <title>Hello Example!</title>
    </head>
    <body>
        <h1>World!</h1>
    </body>
</html>
```

# JSON Parser

The jsonp()function is used to parse the JSON. It is extremely simple to use. Returns the result as a hash.

```
def json = (jsonp("test.json"))
output(json)
```
Output:
```
{
	hello: Hello World, title: Hello Example, 
	hello2: [{test: Test}, {test: Test}]
}
```

# Colorize Your Terminal

The colorize() function is used to color the terminal. When needed, beautiful prints can be obtained by giving colors.

Color example (colorize.ol):
```
colorize("red", "Hello Colorize!")
colorize("blue", "Hello Colorize!")
colorize("green", "Hello Colorize!")
colorize("yellow", "Hello Colorize!")
colorize("grey", "Hello Colorize!")
colorize("black", "Hello Colorize!")
colorize("bold", "Hello Colorize!")
colorize("magenta", "Hello Colorize!")
colorize("white", "Hello Colorize!")
colorize("red", "Hello Colorize!", true)
colorize("blue", "Hello Colorize!", true)
colorize("green", "Hello Colorize!", true)
colorize("yellow", "Hello Colorize!", true)
colorize("grey", "Hello Colorize!", true)
colorize("black", "Hello Colorize!", true)
colorize("bold", "Hello Colorize!", true)
colorize("magenta", "Hello Colorize!", true)
colorize("white", "Hello Colorize!", true);
```

![colorize](/img/colorize_terminal_example.png "Colorize Example")