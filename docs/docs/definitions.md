How to make definitions and details

# Definition

Simple casting definition;

```
def something = something;
```

# Hash

Simple casting hashes;
```
def pyramid = {"x":10, "y":10, "z":10}
pyramid["x"]
10
```


# Array

Simple casting arrays;
```
def triangle = [10,20,30]
triangle[1]
10
```

# Integer

Simple casting integer;
```
def zero = 0
0
```

# String

Simple casting string;
```
def name = "Oytun"
"Oytun"
```

# Functions

Simple casting functions;
```
def sum = fn(x,y){ return x+y; }
sum(10,6)
16
```

# Loops 
Cycles differ from other languages by a semicolon.

loop.ola (loop example):
```
def i = 0;
loop(i>100){ 
  println(i);
  def i = (i+1);
}
def arr = [1,2,3,4,5]
for(arr in k,v){
  println(v)
}
def arrtwo = {"one":1, "two":2};
for(arrtwo in k,v){
  println(k)
  println(v)
}
def i = 0;
while(i<100){ 
  println(i);
  def i = (i+1);
}
```
Output:
```
1
2
3
4
....
100

1
2
3
4
5

one
1
two
2

1
2
3
4
....
100
```